$.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

// Slugify
////////////////////////////////////////////////////
function slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '')             // Trim - from end of text
    .replace(/_/g, '-');            // Replace _ with -
}

// Jquery
////////////////////////////////////////////////////
$(document).ready(function() {

	// Delete Link
	////////////////////////////////////////////////////
	$(".delete-link").click(function(e) {
		e.preventDefault();
		$(this).siblings('form').submit();
	});

	// Close Alert
	////////////////////////////////////////////////////
	$(".alert-close").click(function(e) {
		e.preventDefault();
		$(this).parents(".alert").slideUp(200);
	});

	// Close Alert
	////////////////////////////////////////////////////
	$('.validate').each(function() {
        $(this).validate({
	 		errorElement: "div",
	 		rules: {
	 			phone: { phoneUS: true },
	 			email: { email: true }
	 		}
		});
    });

    // Data Mirror
	////////////////////////////////////////////////////
	$("[data-mirror]").each(function() {
		var e = $(this),
			t = e.text();
		
		$("#" + e.data('mirror')).keyup(function() {
			if ($(this).val()) {
				e.text($(this).val());
			} else {
				e.text(t);
			}
		});
	});

	// Slugify
	////////////////////////////////////////////////////
	$(".slugify").keyup(function() {
		var el = $(this).data('slugify') ? $(this).data('slugify') : '#slug';
		$(el).val(slugify($(this).val()));
	});

	// Toggle and Clear
	////////////////////////////////////////////////////
	$(".toggle").click(function(e) {
		e.preventDefault();
		$("." + $(this).data('toggle')).find(':input').val('').end().toggle();
	});




});