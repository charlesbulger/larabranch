$.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

$(document).ready(function() {

	// Menu Toggle
	/////////////////////////////////////////////////////////////////////////
	$(".menu-toggle").click(function(e) {
		e.preventDefault();
		$(".main-menu").toggleClass('open');
		$("body").toggleClass('menu-open');
	});

	$(document).mouseup(function(e) {
	    var container = $(".main-menu, .menu-toggle");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        $(".main-menu").removeClass('open');
			$("body").removeClass('menu-open');
	    }
	});

	// Close Alert
	////////////////////////////////////////////////////
	$(".alert-close").click(function(e) {
		e.preventDefault();
		$(this).parents(".alert").slideUp(200);
	});

	// Close Alert
	////////////////////////////////////////////////////
	$('.validate').each(function() {
        $(this).validate({
	 		errorElement: "div",
	 		rules: {
	 			phone: { phoneUS: true },
	 			email: { email: true }
	 		}
		});
    });


});