<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = ['city_id', 'slug', 'name'];

    public function city() {
    	return $this->belongsTo('App\City');
    }

    public function locations() {
    	return $this->hasMany('App\Location');
    }

    public static function lists() {
        return self::orderBy('name')->pluck('name', 'id')->all();
    }

    public function getLinkAttribute() {
        return url($this->city->state->slug . '/' . $this->city->slug . '/' . $this->slug);
    }
}
