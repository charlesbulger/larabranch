<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\State;

class StateController extends Controller
{
    public $validation = ['slug' => 'required', 'name' => 'required'];

    public function index() {
    	$states = State::orderBy('name')->get();
    	return view('admin.states.index', compact('states'));
    }

    public function create() {
		return view('admin.states.create');
	}

    public function store(Request $request) {
    	$this->validate($request, $this->validation);
		$state = State::create($request->all());
		return redirect()->route('admin.states.edit', $state->id)->with(['alert' => 'State created.']);
	}

	public function edit(State $state) {
		return view('admin.states.edit', compact('state'));
    }

    public function update(State $state, Request $request) {
		$this->validate($request, $this->validation);
		$state->update($request->all());

		return redirect()->route('admin.states.edit', $state->id)->with(['alert' => 'State saved.']);
	}

	public function destroy(State $state, Request $request) {
		$state->delete();
		return redirect()->back()->with(['alert' => 'State deleted.']);
	}

	public function cities(State $state) {
		return response()->json($state->cities);
	}

	public function businesses(State $state) {
		return response()->json($state->businesses);
	}
}
