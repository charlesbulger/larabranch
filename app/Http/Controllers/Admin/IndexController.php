<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Page;

class IndexController extends Controller
{

    public function show(Request $request) {

    	$counts['pages'] = Page::count();

    	return view('admin.index', compact('counts'));
    }


}
