<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Business;
use App\City;

class BusinessController extends Controller
{
    public $validation = ['slug' => 'required', 'name' => 'required'];

    public function index() {
    	$businesses = Business::latest()->get();
    	return view('admin.businesses.index', compact('businesses'));
    }

    public function create() {
    	$cities = City::lists();
		return view('admin.businesses.create', compact('cities'));
	}

    public function store(Request $request) {
    	$this->validate($request, $this->validation);
		$business = Business::create($request->all());
		return redirect()->route('admin.businesses.edit', $business->id)->with(['alert' => 'Business created.']);
	}

	public function edit(Business $business) {
		$cities = City::lists();
		return view('admin.businesses.edit', compact('business', 'cities'));
    }

    public function update(Business $business, Request $request) {
		$this->validate($request, $this->validation);
		$business->update($request->all());
		return redirect()->route('admin.businesses.edit', $business->id)->with(['alert' => 'Business saved.']);
	}

	public function destroy(Business $business, Request $request) {
		$business->delete();
		return redirect()->back()->with(['alert' => 'Business deleted.']);
	}
}
