<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\City;
use App\State;

class CityController extends Controller
{
    public $validation = ['slug' => 'required', 'name' => 'required'];

    public function index() {
    	$cities = City::with('state')->latest()->get();
    	return view('admin.cities.index', compact('cities'));
    }

    public function create() {
    	$states = State::lists();
		return view('admin.cities.create', compact('states'));
	}

    public function store(Request $request) {
    	$this->validate($request, $this->validation);
		$city = City::create($request->all());
		return redirect()->route('admin.cities.edit', $city->id)->with(['alert' => 'City created.']);
	}

	public function edit(City $city) {
		$states = State::lists();
		return view('admin.cities.edit', compact('city', 'states'));
    }

    public function update(City $city, Request $request) {
		$this->validate($request, $this->validation);
		$city->update($request->all());

		return redirect()->route('admin.cities.edit', $city->id)->with(['alert' => 'City saved.']);
	}

	public function destroy(City $city, Request $request) {
		$city->delete();
		return redirect()->back()->with(['alert' => 'City deleted.']);
	}

	public function businesses(City $city) {
		return response()->json($city->businesses);
	}
}
