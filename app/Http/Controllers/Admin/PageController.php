<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Page;

class PageController extends Controller
{
    public $validation = ['slug' => 'required', 'title' => 'required'];

    public function index() {
    	$pages = Page::latest()->get();
    	return view('admin.pages.index', compact('pages'));
    }

    public function create() {
		return view('admin.pages.create');
	}

    public function store(Request $request) {
    	$this->validate($request, $this->validation);
		$page = Page::create($request->all());
		return redirect()->route('admin.pages.edit', $page->id)->with(['alert' => 'Page created.']);
	}

	public function edit(Page $page) {
		return view('admin.pages.edit', compact('page'));
    }

    public function update(Page $page, Request $request) {
		$this->validate($request, $this->validation);
		$page->update($request->all());

		return redirect()->route('admin.pages.edit', $page->id)->with(['alert' => 'Page saved.']);
	}

	public function destroy(Page $page, Request $request) {
		$page->delete();
		return redirect()->back()->with(['alert' => 'Page deleted.']);
	}
}