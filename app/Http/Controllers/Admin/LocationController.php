<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Business;
use App\City;
use App\Location;
use App\State;

use Geocode;

class LocationController extends Controller
{
    public $validation = [
    	'slug'          => 'required', 
    	'address'       => 'required', 
    	'state_name'    => 'unique:states,name',
    	'state_slug'    => 'unique:states,slug'
   	];

    public function index() {
    	$locations = Location::latest()->paginate(20);
    	return view('admin.locations.index', compact('locations'));
    }

    public function create() {
    	$states     = State::lists();
    	$cities     = [];
    	$businesses = [];
		return view('admin.locations.create', compact('businesses', 'cities', 'states'));
	}

    public function store(Request $request) {
    	$location = $this->save($request);
		return redirect()->route('admin.locations.edit', $location->id)->with(['alert' => 'Location created.']);
	}

	public function edit(Location $location) {
		$states     = State::lists();
    	$cities     = $location->business->city->state->cities->pluck('name', 'id')->all();
    	$businesses = $location->business->city->businesses->pluck('name', 'id')->all();

		return view('admin.locations.edit', compact('location', 'businesses', 'cities', 'states'));
    }

    public function update(Location $location, Request $request) {
    	$location = $this->save($request, $location);
		return redirect()->route('admin.locations.edit', $location->id)->with(['alert' => 'Location saved.']);
	}

	public function destroy(Location $location, Request $request) {
		$location->delete();
		return redirect()->back()->with(['alert' => 'Location deleted.']);
	}

	public function save(Request $request, $location = null) {

		$this->validate($request, $this->validation);

    	// Create or Find State
    	if ($request->state_slug && $request->state_name) {
    		$state = State::create(['slug' => $request->state_slug, 'name' => $request->state_name]);
    	} else {
    		$state = State::findOrFail($request->state_id);
    	}

    	// Create or Find City
    	if ($request->city_slug && $request->city_name) {
    		$city = $state->cities()->create(['slug' => $request->city_slug, 'name' => $request->city_name]);
    	} else {
    		$city = City::where('id', $request->city_id)->where('state_id', $state->id)->firstOrFail();
    	}

		// Create or Find Business
    	if ($request->business_slug && $request->business_name) {
    		$business = $city->businesses()->create(['slug' => $request->business_slug, 'name' => $request->business_name]);
    	} else {
    		$business = Business::where('id', $request->business_id)->where('city_id', $city->id)->firstOrFail();
    	}

    	// Create Location
    	if ($location) {
    		$request->merge(['business_id' => $business->id]);
    		$location->update($request->all());
    	} else {
    		$location = $business->locations()->create($request->all());
    	}

        // Geocode
        $geocode = Geocode::make()->address($location->address_string);
        if ($geocode) {
            $location->update(['lat' => $geocode->latitude(), 'lng' => $geocode->longitude()]);
        }

        return $location;
	}






}
