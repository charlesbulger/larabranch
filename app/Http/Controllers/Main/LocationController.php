<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Location;
use App\Business;
use App\City;
use App\State;
use App\Page;

class LocationController extends Controller
{
    public function index($state_slug, $city_slug = null, $business_slug = null, Request $request) {

    	// Check if there's a page we should be showing:
    	//////////////
    	$page = Page::where('slug', $request->path())->first();
    	if ($page) {
    		return view('main.page', compact('page'));
    	}


    	// Else - list the locations for this combination.
    	//////////////

    	$state     = State::where('slug', $state_slug)->first();
    	$city      = $state ? City::where('slug', $city_slug)->where('state_id', $state->id)->first() : null;
    	$business  = $city ? Business::where('slug', $business_slug)->where('city_id', $city->id)->first() : null;

    	$query = Location::with('business.city.state')->where('active', 1);
    	$query->parents($state, $city, $business);
    	$locations = $query->paginate(20);

    	return view('main.locations.index', compact('locations', 'business', 'city', 'state'));
    }

    public function show($state_slug, $city_slug, $business_slug, $location_slug) {


        $state     = State::where('slug', $state_slug)->firstOrFail();
        $city      = City::where('slug', $city_slug)->where('state_id', $state->id)->firstOrFail();
        $business  = Business::where('slug', $business_slug)->where('city_id', $city->id)->firstOrFail();


        $location  = Location::where('slug', $location_slug)->where('active', 1)->where('business_id', $business->id)->firstOrFail();

        $location->load('business.city.state');

        return view('main.locations.show', compact('location', 'business', 'city', 'state'));

    }
}
