<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Location;

class PageController extends Controller
{
	public function index() {

		$query = Location::with('business.city.state')->where('active', 1);
    	$locations = $query->paginate(20);

    	return view('main.index', compact('locations'));
    }


    
    
}
