<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Location;
use App\Business;
use App\City;
use App\State;

use Geocode;



class ImportClinics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clinics:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import clinics from a .csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }


    public function handle()
    {
        
        $this->info('Starting');


        $csv = 'C:\xampp\htdocs\larabranch\final.csv';
        
        $array = $fields = array(); $i = 0;
        $handle = @fopen($csv, "r");
        if ($handle) {
            while (($row = fgetcsv($handle, 4096)) !== false) {
                if (empty($fields)) {
                    $fields = $row;
                    continue;
                }
                foreach ($row as $k=>$value) {
                    $array[$i][$fields[$k]] = $value;
                }
                $i++;
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }

        // $array is the data now. 

        $row = null;

        // foreach (array_slice($array, 0, 5, true) as $row) {
        foreach ($array as $row) {




            if ($row['state_slug'] != '' && $row['city_name'] != '' && $row['business_name'] != '' && $row['address'] != '' && $row['address'] != ' ') {


                $this->info('Starting Import: ' . $row['full_address']);


                $state    = State::firstOrCreate(['slug' => $row['state_slug']], ['name' => $row['state_name']]);
                $city     = City::firstOrCreate(['state_id' => $state->id, 'slug' => $this->slugify($row['city_name'])], ['name' => $row['city_name']]);
                $business = Business::firstOrCreate(['city_id' => $city->id, 'slug' => $this->slugify($row['business_name'])], ['name' => $row['business_name']]);

                $location = $business->locations()->create([
                    'slug'          => $this->slugify($row['address']),
                    'address'       => $row['address'],
                    'website'       => $row['website'],
                    'phone'         => $row['phone'],
                    'full_address'  => $row['full_address'],
                    'physicians'    => $row['physicians'],
                    'previous_name' => $row['previous_name'],
                    'ivf_rate'      => $row['ivf_rate'],
                    'icsi_rate'     => $row['icsi_rate'],
                    'pgd_rate'      => $row['pgd_rate']
                ]);

                // Geocode
                $geocode = Geocode::make()->address($location->address_string);
                if ($geocode) {
                    $location->update(['lat' => $geocode->latitude(), 'lng' => $geocode->longitude()]);
                }



            }







        }
















    }
}
