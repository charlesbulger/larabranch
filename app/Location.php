<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'business_id', 'slug', 'address', 'lat', 'lng', 'full_address', 'website', 'description', 'phone', 'physicians', 
        'previous_name', 'ivf_rate', 'icsi_rate', 'pgd_rate', 'hours_mon', 'hours_tue', 'hours_wed', 'hours_thu', 'hours_fri', 'hours_sat', 'hours_sun', 'active'];

    public function business() {
    	return $this->belongsTo('App\Business');
    }

    public function getLinkAttribute() {
       	return url($this->business->city->state->slug . '/' . $this->business->city->slug . '/' . $this->business->slug . '/' . $this->slug);
    }

    public function getAddressStringAttribute() {
        return $this->address . ', ' . $this->business->city->name . ', ' . $this->business->city->state->name;
    }

    public function getStateIdAttribute($value) {
    	return $this->business->city->state->id;
    }

    public function getCityIdAttribute($value) {
    	return $this->business->city->id;
    }


    public function scopeBusiness($query, $business) {
        if ($business) {
            $query->whereHas('business', function ($q) use ($business) {
                $q->where('id', '=', $business->id);
            });
        }
    }

    public function scopeCity($query, $city) {
        if ($city) {
            $query->whereHas('business.city', function($q) use ($city) {
                $q->where('id', '=', $city->id);
            });
        }
    }

    public function scopeState($query, $state) {
        $query->whereHas('business.city.state', function($q) use ($state) {
            $q->where('id', '=', $state->id);
        });
    }

    public function scopeParents($query, $state, $city, $business) {
        $query->state($state);
        $query->city($city);
        $query->business($business);
    }


}