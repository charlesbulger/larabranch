<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*----------- 
|-Admin 
|------------*/
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::get('/',                         ['as' => 'index', 'uses' => 'IndexController@show']);
    Route::get('cities/{city}/businesses',  ['as' => 'cities.businesses', 'uses' => 'CityController@businesses']);
    Route::get('states/{state}/cities',     ['as' => 'states.cities', 'uses' => 'StateController@cities']);
    Route::get('states/{state}/businesses', ['as' => 'states.businesses', 'uses' => 'StateController@businesses']);

    Route::resource('businesses', 'BusinessController', ['except' => ['show']]);
    Route::resource('cities',     'CityController',     ['except' => ['show']]);
    Route::resource('locations',  'LocationController', ['except' => ['show']]);
    Route::resource('pages',      'PageController',     ['except' => ['show']]);
    Route::resource('states',     'StateController',    ['except' => ['show']]);
    Route::resource('users',      'UserController',     ['except' => ['show']]);
});


/*----------- 
|-Main 
|------------*/
Route::group(['namespace' => 'Main', 'as' => 'main.'], function () {


    Route::get('/', ['as' => 'index', 'uses'  => 'PageController@index']);

    Route::get('{state}/{city}/{business}/{location}', ['as' => 'locations.show', 'uses'  => 'LocationController@show']);
    Route::get('{state}/{city?}/{business?}/',         ['as' => 'locations.index', 'uses'  => 'LocationController@index']);



});