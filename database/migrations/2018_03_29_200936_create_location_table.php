<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('slug');
            $table->string('name');
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('restrict');

            $table->string('slug');
            $table->string('name');
        });

        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('restrict');

            $table->string('slug');
            $table->string('name');
        });

        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('business_id')->unsigned();
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('restrict');

            $table->string('slug');
            $table->string('address');

            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            
            $table->string('full_address')->nullable();

            $table->string('website')->nullable();
            $table->text('description')->nullable();
            $table->string('phone')->nullable();
            $table->text('physicians')->nullable();
            $table->string('previous_name')->nullable();
            $table->string('ivf_rate')->nullable();
            $table->string('icsi_rate')->nullable();
            $table->string('pgd_rate')->nullable();

            $table->text('hours_mon')->nullable();
            $table->text('hours_tue')->nullable();
            $table->text('hours_wed')->nullable();
            $table->text('hours_thu')->nullable();
            $table->text('hours_fri')->nullable();
            $table->text('hours_sat')->nullable();
            $table->text('hours_sun')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
        Schema::dropIfExists('businesses');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('states');
    }
}