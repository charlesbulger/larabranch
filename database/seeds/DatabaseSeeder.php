<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Charlie',
            'email'    => 'charlesmbulger@gmail.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'name'     => 'Nate',
            'email'    => 'natebro21@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
