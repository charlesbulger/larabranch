@extends('main.app', ['title' => 'Clinic Locator - Your Fertility Friend'])


@section('content')

	<h1>Clinic Locator</h1>
	@include('main.locations.partials.list')

@stop