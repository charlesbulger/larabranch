<!doctype html>
<html>
	<head>
		<title>{{ isset($title) ? $title : 'Your Fertility Friend' }}</title>

		<!-- meta -->
		<meta charset="utf-8">
		<base href="https://yourfertilityfriend.com/clinics/">
		<link rel="shortcut icon" href="static/main/images/favicon.png?v=2">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- css -->
		<link rel="stylesheet" type="text/css" href="static/main/css/style.css?r={{ rand(1,999) }}">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link href="https://fonts.googleapis.com/css?family=Amatic+SC:700|Montserrat:300,400,400i,500,600,600i" rel="stylesheet">

		<!-- csrf -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!--[if lt IE 9]>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->

		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		     (adsbygoogle = window.adsbygoogle || []).push({
		          google_ad_client: "ca-pub-5667824922574301",
		          enable_page_level_ads: true
		     });
		</script>
	</head>
	<body>
	
		<header>
			<section class="header-top">
				<div class="c">
					<span class="call">Get Help Today: <a href="tel:+18333743631">1-833-374-3631</a></span>
					<ul id="menu-top-menu" class="top-menu">
						<li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358"><a href="https://yourfertilityfriend.com/about-us/">About Us</a></li>
						<li id="menu-item-357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357"><a href="https://yourfertilityfriend.com/contact-us/">Contact Us</a></li>
					</ul>
				</div><!-- c -->
			</section><!-- header-top -->
			<section class="header-main">
				<div class="c">
					<a href="/" id="logo"><img src="static/main/images/your-fertility-friend.png" alt="Your Fertility Friend"></a>
					<a class="menu-toggle" href="javascript:;"><i class="fa fa-bars"></i></a>
					<ul id="menu-main-menu" class="main-menu">
						<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://yourfertilityfriend.com/ivf-process/">The IVF Process</a></li>
						<li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://yourfertilityfriend.com/ivf-costs/">IVF Costs</a></li>
						<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="https://yourfertilityfriend.com/ivf-success-rates/">IVF Success Rates</a></li>
						<li id="menu-item-402" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-402"><a href="https://yourfertilityfriend.com/clinics">Clinics</a></li>
						<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="https://yourfertilityfriend.com/resources/">Resources</a></li>
						<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://yourfertilityfriend.com/get-help-now/">Get Help Now</a></li>
					</ul>
				</div><!-- c -->
			</section><!-- header-main -->
		</header>

		@include('main.partials.breadcrumbs')
		<div class="main">
			<div class="c">	
				@yield('content')
			</div><!--c -->
		</div><!-- main -->	

		<footer>
			<section class="footer-main">
				<div class="c">
					<a href="/" class="logo"><img src="static/main/images/your-fertility-friend.png" alt="Your Fertility Friend"></a>
					<ul id="menu-main-menu" class="main-menu">
						<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://yourfertilityfriend.com/ivf-process/">The IVF Process</a></li>
						<li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://yourfertilityfriend.com/ivf-costs/">IVF Costs</a></li>
						<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="https://yourfertilityfriend.com/ivf-success-rates/">IVF Success Rates</a></li>
						<li id="menu-item-402" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-402"><a href="https://yourfertilityfriend.com/clinics">Clinics</a></li>
						<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="https://yourfertilityfriend.com/resources/">Resources</a></li>
						<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://yourfertilityfriend.com/get-help-now/">Get Help Now</a></li>
					</ul>
					
					<div class="grid clear">
						<div class="col-2-3">
							<h3>Website Disclaimer</h3>
							<p class="disclaimer">YourFertilityFriend.com is a privately-owned website. It is designed for educational purposes only and is not to be used as medical advice. The information provided through this website should not be used for diagnosing or treating a health problem or disease. It is not a substitute for professional care. If you have or suspect you may have a health problem, you should consult your health care provider. The authors, editors, producers, sponsors, and contributors shall have no liability, obligation, or responsibility to any person or entity for any loss, damage, or adverse consequences alleged to have happened directly or indirectly as a consequence of material on this website. If you believe you have a medical emergency, you should immediately call 911.</p>
						</div>
						<div class="col-1-3">
							<h3>Connect With Us</h3>
							<ul>
								<li><a href="">Facebook</a></li>
							</ul>
						</div>	
					</div><!-- grid -->
				</div><!-- c -->
			</section><!-- footer-main -->
			<section class="footer-bottom">
				<div class="c">
					<p>Copyright &copy; <?=date('Y'); ?> Your Fertility Friend LLC</p>
					<ul id="menu-footer-menu" class="menu">
						<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="https://yourfertilityfriend.com/about-us/">About</a></li>
						<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52"><a href="https://yourfertilityfriend.com/contact-us/">Contact Us</a></li>
						<li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50"><a href="https://yourfertilityfriend.com/terms/">Terms and Conditions</a></li>
						<li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="https://yourfertilityfriend.com/privacy/">Privacy Policy</a></li>
						<li id="menu-item-386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-386"><a href="https://yourfertilityfriend.com/sitemap.xml">Sitemap</a></li>
					</ul>
				</div><!-- c -->
			</section><!-- footer-bottom -->
		</footer>


		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
		<script type="text/javascript" src="static/main/js/app.js"></script>
	</body>
</html>