<div class="location-preview">
	<h1><a href="{{ $location->link }}">{{ $location->business->name }}</a></h1>
	<p>{{ $location->address }}<br><a href="{{ $location->business->city->link }}">{{ $location->business->city->name }}</a>, <a href="{{ $location->business->city->state->link }}">{{ $location->business->city->state->name }}</a></p>

</div>