@extends('main.app', ['title' => $location->business->name . ' - Your Fertility Friend'])


@section('content')
	<div class="grid">
		<div class="col-2-3">
			<h1>{{ $location->business->name }}</h1>
			<h3>{{ $location->address }}<br><a href="{{ $location->business->city->link }}">{{ $location->business->city->name }}</a>, <a href="{{ $location->business->city->state->link }}">{{ $location->business->city->state->name }}</a></h3>
			
			
			<p><b>Full Address: </b>{{ $location->full_address }}</p>
			<p><b>Website: </b><a target="_blank" href="{{ $location->website }}">{{ $location->website }}</a></p>
			<p><b>Phone: </b>{{ $location->phone }}</p>

			@if ($location->previous_name)
				<p><b>Previous Name: </b>{{ $location->previous_name }}</p>
			@endif

			<h3>Rates</h3>
			<table>
				<tbody>
					<tr>
						<td>IVF Rate</td>
						<td>{{ $location->ivf_rate }}</td>
					</tr>
					<tr>
						<td>ICSI Rate</td>
						<td>{{ $location->icsi_rate }}</td>
					</tr>
					<tr>
						<td>PGD Rate</td>
						<td>{{ $location->pgd_rate }}</td>
					</tr>
				</tbody>
			</table>

			<h3>Physicians</h3>
			<p>{!! $location->physicians !!}</p>

			@if ($location->description)
				<h3>Description</h3>
				{!! $location->description !!}
			@endif

		</div><!-- col-2-3 -->
		<div class="col-1-3">
			@include('main.partials.map-single')
			@include('main.partials.adsense')
		</div><!-- col-1-3 -->
	</div><!-- grid -->
@stop