@extends('admin.app')

@section('content')

    <h1>Reset Password</h1>

    {{ Form::open(['route' => 'password.request', 'class' => 'validate']) }}

        {{ Form::hidden('token', $token) }}

        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', $email or old('email'), ['required' => true]) }}

        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', ['required' => true]) }}

        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation', ['required' => true]) }}

        {{ Form::submit('Reset Password') }}

    {{ Form::close() }}

@endsection
