@extends('admin.app', ['title' => 'Cities'])




@section('content')
	<a class="btn float-right" href="{{ route('admin.cities.create') }}">Create City</a>
	<h1>Cities</h1>
	@include('admin.cities._table', ['cities' => $cities])
@stop