@extends('admin.app', ['title' => 'Edit City: ' . $city->slug])



@section('content')
	<h1><a href="{{ route('admin.cities.index') }}">Cities</a> / Edit City: <span data-mirror="slug">{{ $city->slug }}</span></h1>
	{{ Form::model($city, ['route' => ['admin.cities.update', $city->id], 'method' => 'PATCH', 'class' => 'validate']) }}
		@include('admin.cities._form')
	{{ Form::close() }}
@stop