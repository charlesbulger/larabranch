@extends('admin.app', ['title' => 'Create a City'])



@section('content')
	<h1><a href="{{ route('admin.cities.index') }}">Cities</a> / Create</h1>
	{{ Form::open(['route' => 'admin.cities.store', 'class' => 'validate']) }}
		@include('admin.cities._form')
	{{ Form::close() }}
@stop