@extends('admin.app', ['title' => 'Edit: ' . $user->name])


@section('content')
	<h1><a href="{{ route('admin.users.index') }}">Users</a> / Edit User: <span data-mirror="name">{{ $user->name }}</span></h1>
	{{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PATCH', 'files' => true]) }}
		@include('admin.users._form')
	{{ Form::close() }}
@stop