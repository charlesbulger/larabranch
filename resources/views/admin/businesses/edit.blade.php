@extends('admin.app', ['title' => 'Edit Business: ' . $business->slug])



@section('content')
	<h1><a href="{{ route('admin.businesses.index') }}">Businesses</a> / Edit Business: <span data-mirror="slug">{{ $business->slug }}</span></h1>
	{{ Form::model($business, ['route' => ['admin.businesses.update', $business->id], 'method' => 'PATCH', 'class' => 'validate']) }}
		@include('admin.businesses._form')
	{{ Form::close() }}
@stop