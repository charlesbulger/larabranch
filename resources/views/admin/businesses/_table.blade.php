<table>
	<thead>
		<th>Name</th>
		<th>Slug</th>
		<th>City</th>
		<th>State</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($businesses as $business)
			<tr>
				<td><a href="{{ route('admin.businesses.edit', $business->id) }}">{{ $business->name }}</a></td>
				<td>{{ $business->slug }}</td>
				<td>{{ $business->city->name }}</td>
				<td>{{ $business->city->state->name }}</td>
				<td>
					<a href="{{ route('admin.businesses.edit', $business->id ) }}">Edit</a> | 
					<a class="delete-link" href="javascript:;">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.businesses.destroy', $business->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>