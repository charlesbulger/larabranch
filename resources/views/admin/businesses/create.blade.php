@extends('admin.app', ['title' => 'Create a Business'])



@section('content')
	<h1><a href="{{ route('admin.businesses.index') }}">Businesses</a> / Create</h1>
	{{ Form::open(['route' => 'admin.businesses.store', 'class' => 'validate']) }}
		@include('admin.businesses._form')
	{{ Form::close() }}
@stop