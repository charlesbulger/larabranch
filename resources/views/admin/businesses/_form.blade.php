<fieldset>
	{{ Form::label('city_id', 'City') }}
	{{ Form::select('city_id', [null => 'Select'] + $cities, null, ['required' => true]) }}
</fieldset>

<fieldset>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', null, ['required' => true, 'class' => 'slugify']) }}
</fieldset>

<fieldset>
	{{ Form::label('slug', 'Slug') }}
	{{ Form::text('slug', null, ['required' => true]) }}
</fieldset>

{{ Form::submit('Save') }}