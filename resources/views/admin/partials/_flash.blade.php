@if (session('alert'))
	<div class="alert {{ session('alert-class') ?: 'alert-success '}}"><a href="javascript:;" class="alert-close"><i class="fa fa-close"></i></a>{{ session('alert') }}</div>
@endif