@extends('admin.app', ['title' => 'States'])




@section('content')
	<a class="btn float-right" href="{{ route('admin.states.create') }}">Create State</a>
	<h1>States</h1>
	@include('admin.states._table', ['states' => $states])
@stop