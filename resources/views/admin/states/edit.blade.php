@extends('admin.app', ['title' => 'Edit State: ' . $state->slug])



@section('content')
	<h1><a href="{{ route('admin.states.index') }}">States</a> / Edit State: <span data-mirror="slug">{{ $state->slug }}</span></h1>
	{{ Form::model($state, ['route' => ['admin.states.update', $state->id], 'method' => 'PATCH', 'class' => 'validate']) }}
		@include('admin.states._form')
	{{ Form::close() }}
@stop