@extends('admin.app', ['title' => 'Locations'])




@section('content')
	<a class="btn float-right" href="{{ route('admin.locations.create') }}">Create Location</a>
	<h1>Locations</h1>
	@include('admin.locations._table', ['locations' => $locations])

	{{ $locations->appends(request()->except('page'))->links() }}
@stop