@extends('admin.app', ['title' => $location->slug])



@section('content')
	<h1><a href="{{ route('admin.locations.index') }}">Locations</a> / <span data-mirror="slug">{{ $location->slug }}</span></h1>
	{{ Form::model($location, ['route' => ['admin.locations.update', $location->id], 'method' => 'PATCH', 'class' => 'validate']) }}
		@include('admin.locations._form')
	{{ Form::close() }}
@stop