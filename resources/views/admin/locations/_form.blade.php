<h3>Geography</h3>
@if (isset($location))
	<p><a target="_blank" href="{{ $location->link }}">{{ $location->link }}</a></p>
@endif

<fieldset>
	{{ Form::hidden('active', 0) }}
	{{ Form::checkbox('active', 1, null, ['id' => 'active']) }}
	{{ Form::label('active', 'Active') }}
</fieldset>

<div class="grid">
	<div class="col-4-5">
		<div class="well">
			<div class="grid">
				<div class="col-1-4">
					<div class="state-fields">
						<fieldset>
							{{ Form::label('state_id', 'State') }}
							{{ Form::select('state_id', [null => 'Select'] + $states, null, ['required' => true]) }}
						</fieldset>
					</div><!-- state-fields -->
					<div class="state-fields hidden">
						<fieldset>
							{{ Form::label('state_name', 'State Name') }}
							{{ Form::text('state_name', null, ['placeholder' => 'Ohio', 'required' => true]) }}
						</fieldset>
						<fieldset>
							{{ Form::label('state_slug', 'State Slug') }}
							{{ Form::text('state_slug', null, ['placeholder' => 'oh', 'required' => true]) }}
						</fieldset>
					</div><!-- state-fields -->
					<p>
						<a href="javascript:;" class="toggle state-fields create-state" data-toggle="state-fields">or, Create New</a>
						<a href="javascript:;" class="toggle state-fields hidden" data-toggle="state-fields">or, Select Existing</a>
					</p>
				</div><!-- col-1-4 -->

				<div class="col-1-4">
					<div class="city-fields">
						<fieldset>
							{{ Form::label('city_id', 'City') }}
							{{ Form::select('city_id', [null => 'Select'] + $cities, null, ['required' => true]) }}
						</fieldset>
					</div><!-- state-fields -->
					<div class="city-fields hidden city-create">
						<fieldset>
							{{ Form::label('city_name', 'City Name') }}
							{{ Form::text('city_name', null, ['class' => 'slugify', 'data-slugify' => '#city_slug', 'required' => true]) }}
						</fieldset>
						<fieldset>
							{{ Form::label('city_slug', 'City Slug') }}
							{{ Form::text('city_slug', null, ['required' => true]) }}
						</fieldset>
					</div><!-- city-fields -->
					<p>
						<a href="javascript:;" class="toggle city-fields create-city" data-toggle="city-fields">or, Create New</a>
						<a href="javascript:;" class="toggle city-fields hidden city-create" data-toggle="city-fields">or, Select Existing</a>
					</p>
				</div><!-- col-1-4 -->

				<div class="col-1-4">
					<div class="business-fields">
						<fieldset>
							{{ Form::label('business_id', 'Business') }}
							{{ Form::select('business_id', [null => 'Select'] + $businesses, null, ['required' => true]) }}
						</fieldset>
					</div><!-- business-fields -->
					<div class="business-fields hidden business-create">
						<fieldset>
							{{ Form::label('business_name', 'Business Name') }}
							{{ Form::text('business_name', null, ['class' => 'slugify', 'data-slugify' => '#business_slug', 'required' => true]) }}
						</fieldset>
						<fieldset>
							{{ Form::label('business_slug', 'Business Slug') }}
							{{ Form::text('business_slug', null, ['required' => true]) }}
						</fieldset>
					</div><!-- business-fields -->
					<p>
						<a href="javascript:;" class="toggle business-fields" data-toggle="business-fields">or, Create New</a>
						<a href="javascript:;" class="toggle business-fields hidden business-create" data-toggle="business-fields">or, Select Existing</a>
					</p>
				</div><!-- col-1-4 -->

				<div class="col-1-4">
					<fieldset>
						{{ Form::label('address', 'Address') }}
						{{ Form::text('address', null, ['required' => true, 'class' => 'slugify']) }}
					</fieldset>

					<fieldset>
						{{ Form::label('slug', 'Slug') }}
						{{ Form::text('slug', null, ['required' => true]) }}
					</fieldset>
				</div><!-- col-1-4 -->
			</div><!-- grid -->
		</div><!-- well -->

		<fieldset>
			{{ Form::label('full_address', 'Full Address') }}
			{{ Form::text('full_address') }}
		</fieldset>

		<div class="grid">
			<div class="col-1-2">
				<h3>Information</h3>
				<fieldset>
					{{ Form::label('website', 'Website') }}
					{{ Form::text('website', null, ['placeholder' => 'http://']) }}
				</fieldset>
				<fieldset>
					{{ Form::label('phone', 'Phone Number') }}
					{{ Form::text('phone', null, ['placeholder' => 'xxx-xxx-xxxx']) }}
				</fieldset>
				<fieldset>
					{{ Form::label('physicians', 'Physicians Name(s)') }}
					{{ Form::text('physicians') }}
				</fieldset>
				<fieldset>
					{{ Form::label('previous_name', 'Previous Clinic Name') }}
					{{ Form::text('previous_name') }}
				</fieldset>
				<fieldset>
					{{ Form::label('ivf_rate', 'IVF Rate') }}
					{{ Form::text('ivf_rate') }}
				</fieldset>
				<fieldset>
					{{ Form::label('icsi_rate', 'ICSI Rate') }}
					{{ Form::text('icsi_rate') }}
				</fieldset>
				<fieldset>
					{{ Form::label('pgd_rate', 'PGD Rate') }}
					{{ Form::text('pgd_rate') }}
				</fieldset>
				<fieldset>
					{{ Form::label('description', 'Description') }}
					{{ Form::textarea('description') }}
				</fieldset>
			</div><!-- col-1-2- -->
			<div class="col-1-2">
				<h3>Hours</h3>
				<fieldset>
					{{ Form::label('hours_mon', 'Monday') }}
					{{ Form::text('hours_mon') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_tue', 'Tuesday') }}
					{{ Form::text('hours_tue') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_wed', 'Wednesday') }}
					{{ Form::text('hours_wed') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_thu', 'Thursday') }}
					{{ Form::text('hours_thu') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_fri', 'Friday') }}
					{{ Form::text('hours_fri') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_sat', 'Saturday') }}
					{{ Form::text('hours_sat') }}
				</fieldset>
				<fieldset>
					{{ Form::label('hours_sun', 'Sunday') }}
					{{ Form::text('hours_sun') }}
				</fieldset>
			</div><!-- col-1-2- -->
		</div><!-- grid -->


	</div><!-- col-4-5 -->
	<div class="col-1-5">
		@if (isset($location))
  			<iframe class="map" src="https://www.google.com/maps/embed/v1/view?key=AIzaSyBs80mQSfGpwGbhFrBh5OxqNlc0tvWUG6g&center={{ $location->lat }},{{ $location->lng }}&zoom=14"></iframe>
  		@endif
	</div><!-- col-1-5 -->
</div><!-- grid -->

{{ Form::submit('Save') }}


@section('scripts')
	<script>
		$(document).ready(function() {
			$("#state_id").change(function() {
				if ($(this).val() != '') {
					$.getJSON('admin/states/' + $(this).val() + '/cities', function(cities) {
						var options = "<option value=''>Select</option>";
						$.each(cities, function(key, city) {
						    options += '<option value=' + city.id + '>' + city.name + '</option>';
						});
						$("#city_id").html(options);
					});

					$.getJSON('admin/states/' + $(this).val() + '/businesses', function(businesses) {
						var options = "<option value=''>Select</option>";
						$.each(businesses, function(key, business) {
						    options += '<option value=' + business.id + '>' + business.name + '</option>';
						});
						$("#business_id").html(options);
					});
				}
			});

			$("#city_id").change(function() {
				if ($(this).val() != '') {
					$.getJSON('admin/cities/' + $(this).val() + '/businesses', function(businesses) {
						var options = "<option value=''>Select</option>";
						$.each(businesses, function(key, business) {
						    options += '<option value=' + business.id + '>' + business.name + '</option>';
						});
						$("#business_id").html(options);
					});
				}
			});

			$(".create-state").click(function() {
				$('.city-fields, .business-fields').hide();
				$('.city-create, .business-create').show();
			});

			$(".create-city").click(function() {
				$('.business-fields').hide();
				$('.business-create').show();
			});
		});
	</script>
@stop