<table>
	<thead>
		<th>Active</th>
		<th>Address</th>
		<th>Business</th>
		<th>City</th>
		<th>State</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($locations as $location)
			<tr>
				<td>{{ $location->active ? 'Active' : '' }}</td>
				<td>
					<a href="{{ route('admin.locations.edit', $location->id) }}">{{ $location->address }}</a><br>
					<span class="small"><a target="_blank" href="{{ $location->link }}">{{ $location->slug }} <i class="fa fa-external-link"></i></a></span>
				</td>
				<td>{{ $location->business->name }}</td>
				<td>{{ $location->business->city->name }}</td>
				<td>{{ $location->business->city->state->name }}</td>
				<td>
					<a target="_blank" href="{{ $location->link }}">View <i class="fa fa-external-link"></i></a> |
					<a href="{{ route('admin.locations.edit', $location->id ) }}">Edit</a> | 
					<a class="delete-link" href="javascript:;">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.locations.destroy', $location->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>