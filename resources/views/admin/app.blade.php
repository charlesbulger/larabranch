<!doctype html>
<html>
	<head>
		<title>{{ isset($title) ? $title : 'Gift' }}</title>

		<!-- meta -->
		<meta charset="utf-8">
		<base href="<?= url('/'); ?>/">
		<link rel="shortcut icon" href="static/admin/images/favicon.png">

		<!-- css -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link rel="stylesheet" href="static/admin/css/style.css?r={{ rand(1,999) }}">

		<!-- csrf -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!--[if lt IE 9]>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<ul class="nav">
			<li><a href="{{ route('admin.index') }}">Dashboard</a></li>
			<li><a href="{{ route('admin.pages.index') }}">Pages</a></li>
			<li>
				<a href="{{ route('admin.locations.index') }}">Locations</a>
				<ul>
					<li><a href="{{ route('admin.businesses.index') }}">Businesses</a></li>
					<li><a href="{{ route('admin.cities.index') }}">Cities</a></li>
					<li><a href="{{ route('admin.states.index') }}">States</a></li>
				</ul>
			</li>
			<li><a href="{{ route('admin.users.index') }}">Admin Users</a></li>
			<li>
				<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
			</li>
		</ul>

		@include('admin.partials._flash')
		@include('admin.partials._errors')
		@yield('content')

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
		<script src="static/admin/js/app.js"></script>
		@yield('scripts')
	</body>
</html>